import React, { useEffect, useState, useCallback } from "react";
import { Button, Form, Card } from "react-bootstrap";

import {
  HttpGetTodo,
  HttpPostTodo,
  HttpDelTodo,
  HttpPutTodo,
} from "../../helper/index";

import { Main, MyForm, MyCard, Overlay } from "../../styles/MainLayout";
import "../../styles/main.css";

export default function HomeIndex() {
  let [list, setList] = useState();
  let [id, setId] = useState(0);
  let [title, setTitle] = useState("");
  let [description, setDescription] = useState("");

  let [show, setShow] = useState(false);
  let [editForm, setEditForm] = useState(false);
  let [error, setError] = useState();

  const handleTitle = (event) => {
    setTitle(event.target.value);
  };

  const handleDes = (event) => {
    setDescription(event.target.value);
  };

  const createTodo = () => {
    setShow(!show);
  };

  const closeAll = () => {
    setEditForm(false);
    setShow(false);
  };

  const getTodo = useCallback(async () => {
    try {
      const response = await HttpGetTodo("todos");
      let resArray = response.data;
      if (resArray.length > 0) {
        setList(resArray);
      }
    } catch (error) {
      setError(true);
    }
  }, []);

  useEffect(() => {
    getTodo();
  }, [getTodo]);

  const postTodoList = async () => {
    if (title && description) {
      let obj = {
        title: title,
        description: description,
      };
      let response = await HttpPostTodo("todos", obj);
      let resArray = response.res.data;
      if (resArray.length > 0) {
        createTodo();
        getTodo();
        setTitle("");
        setDescription("");
      } else {
        createTodo();
        getTodo();
        setTitle("");
        setDescription("");
      }
    } else {
      alert("Please insert your todo!");
    }
  };

  const remove = async (res) => {
    let response = await HttpDelTodo("todos/" + res);
    if (response.status === 200) {
      alert("Delete Success.");
      getTodo();
    } else {
      alert("Delete failed.");
      getTodo();
    }
  };

  const popupEdit = async (res) => {
    setEditForm(true);
    createTodo();
    setId(res._id);
    setTitle(res.title);
    setDescription(res.description);
  };

  const update = async () => {
    try {
      let obj = {
        title: title,
        description: description,
      };
      let response = await HttpPutTodo("todos/" + id, obj);
      setEditForm(false);
      createTodo();
      if (response.status === 200) {
        alert("Update Success.");
        getTodo();
        setTitle("");
        setDescription("");
      }
    } catch (error) {
      setError(true);
      setTitle("");
      setDescription("");
    }
  };

  return (
    <Main>
      {list &&
        list.map((res, index) => (
          <Card
            key={index}
            border="info"
            className="mb-2"
            style={{ maxWidth: "1200px" }}
          >
            <Card.Body>
              <Card.Title className="text-left">
                <b>{res.title}</b>
              </Card.Title>
              <Card.Text className="text-left">{res.description}</Card.Text>
              <Card.Text>{error}</Card.Text>
              <div style={{ display: "flex" }}>
                <Button
                  className="mr-2"
                  variant="outline-info"
                  onClick={() => popupEdit(res)}
                >
                  Edit
                </Button>
                <Button
                  variant="outline-warning"
                  onClick={() => remove(res._id)}
                >
                  Remove
                </Button>
              </div>
            </Card.Body>
          </Card>
        ))}

      <div className={show ? "popup" : "hide"}>
        <MyCard>
          <MyForm>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Title</Form.Label>
              <Form.Control
                type="text"
                placeholder="Title"
                value={title}
                onChange={handleTitle}
              />
            </Form.Group>
            <Form.Group controlId="exampleForm.ControlTextarea1">
              <Form.Label>Description</Form.Label>
              <Form.Control
                as="textarea"
                rows="3"
                placeholder="Description"
                value={description}
                onChange={handleDes}
              />
            </Form.Group>
            <Button
              className={editForm ? "hide" : "popup"}
              variant="primary"
              type="submit"
              onClick={postTodoList}
            >
              Submit
            </Button>

            <Button
              className={editForm ? "popup" : "hide"}
              variant="primary"
              type="submit"
              onClick={update}
            >
              Update
            </Button>
          </MyForm>
        </MyCard>
        <Overlay onClick={closeAll} />
      </div>
      <Button className="mt-4" variant="outline-primary" onClick={createTodo}>
        CREATE
      </Button>
    </Main>
  );
}
