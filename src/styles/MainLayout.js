import { MEDIA_MEDIUM_UP } from "../utils/theme.js";
import styled from "styled-components";

export const Page = styled.div`
  text-align: center;
  width: 100%;
  padding: 0 !important;
  margin: 0 !important;
  font-size: 1.4em;
`;

export const Main = styled.div`
  margin-top: 8px;
  text-align: -webkit-center;
  font-size: 0.8em;
  padding: 8px;

  @media ${MEDIA_MEDIUM_UP} {
    margin-top: 8px;
    text-align: -webkit-center;
    font-size: 1.2em;
    padding: 8px;
  }
`;

export const MyForm = styled.div`
  width: 100%;
  text-align: left;

  @media ${MEDIA_MEDIUM_UP} {
    width: 400px;
    text-align: left;
  }
`;

export const TxtLink = styled.p`
  padding: 0px 8px;
  margin: 0px;
  color: #fff;
  text-align: left;

  @media ${MEDIA_MEDIUM_UP} {
    padding: 0px 8px;
    margin: 0px;
    color: #fff;
  }
`;

export const MyCard = styled.div`
  position: absolute;
  top: 56px;
  /* left: 16px; */
  /* right: 16px; */
  text-align: -webkit-center;
  background-color: #fff;
  z-index: 20;
  width: 98%;
  height: auto;
  padding: 16px;
  border-radius: 4px;
  @media ${MEDIA_MEDIUM_UP} {
    position: absolute;
    top: 80px;
    /* left: 0px; */
    /* right: 0px; */
    text-align: -webkit-center;
    background-color: #fff;
    z-index: 20;
    width: 500px;
    height: auto;
    padding: 24px 8px;
    border-radius: 4px;
  }
`;

export const Overlay = styled.div`
  background-color: rgba(0, 0, 0, 0.7);
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 9;
`;
