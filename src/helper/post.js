import axios from "axios";
import { url } from "../services";

async function HttpPostLogin(api, obj) {
  let token = await localStorage.getItem("token");

  let data = {};
  try {
    const auth = { headers: { Authorization: token } };

    let res = await axios.post(url + `/${api}`, obj, auth);
    data = { res, status: "success" };
  } catch (e) {
    data = { status: "error", massage: e };
  } finally {
    return data;
  }
}

export default HttpPostLogin;
