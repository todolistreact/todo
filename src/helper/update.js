import axios from "axios";
import { url } from "../services";

async function HttpPutTodo(api, obj) {
  const token = await localStorage.getItem("token");
  try {
    const auth = { headers: { Authorization: token } };
    return await axios.put(url + `/${api}`, obj, auth);
  } catch (e) {
    return e;
  }
}

export default HttpPutTodo;
