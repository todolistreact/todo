import axios from "axios";
import { url } from "../services";

async function HttpDelTodo(api) {
  const token = await localStorage.getItem("token");
  try {
    const auth = { headers: { Authorization: token } };
    return await axios.delete(url + `/${api}`, auth);
  } catch (e) {
    return e;
  }
}

export default HttpDelTodo;
